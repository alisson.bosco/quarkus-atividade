
package org.acme.resources;

import javax.ws.rs.*;
import org.acme.entites.Cliente;
import org.acme.services.ClienteService;
import javax.transaction.Transactional;
import javax.inject.Inject;
import java.util.List;

@Path( "/clientes" )
@Produces( "application/json" )
@Consumes( "application/json" )
public class ClienteResource {

    @Inject
    ClienteService clienteService;

    @GET
    public List< Cliente > listarClientes() {
        return clienteService.listarClientes();
    }

    @GET
    @Path( "/{cpf}" )
    public Cliente buscarClientePorCpf( @PathParam( "cpf" ) String cpf ) {
        return clienteService.buscarClientePorCpf( cpf );
    }

    @GET
    @Path( "/{email}" )
    public Cliente buscarClientePorEmail( @PathParam( "email" ) String email ) {
        return clienteService.buscarClientePorEmail( email );
    }

    @POST
    @Transactional
    public void cadastrarCliente( Cliente cliente ) {
        clienteService.cadastrarCliente( cliente );
    }

    @PUT
    @Transactional
    public void atualizarCliente( Cliente cliente ) {
        clienteService.atualizarCliente( cliente );
    }

}