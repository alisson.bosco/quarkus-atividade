
package org.acme.resources;

import javax.ws.rs.*;
import org.acme.entites.Pedido;
import org.acme.services.PedidoService;
import javax.inject.Inject;
import java.util.List;

@Path( "/pedidos" )
@Produces( "application/json" )
@Consumes( "application/json" )
public class PedidoResource {

    @Inject
    PedidoService pedidoService;

    @GET
    public List< Pedido > listarPedidos() {
        return pedidoService.listarPedidos();
    }

    @GET
    @Path( "/{status}" )
    public List< Pedido > listarPedidosPorStatus( @PathParam( "status" ) String status ) {
        return pedidoService.listarPedidosPorStatus( status );
    }
}