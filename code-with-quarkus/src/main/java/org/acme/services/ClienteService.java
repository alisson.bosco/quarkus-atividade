
package org.acme.services;

import javax.enterprise.context.ApplicationScoped;
import org.acme.entites.Cliente;
import java.util.List;

@ApplicationScoped
public class ClienteService {

    public List< Cliente > listarClientes() {
        return Cliente.listAll();
    }

    public Cliente buscarClientePorCpf( String cpf ) {
        return Cliente.find( "cpf", cpf ).firstResult();
    }

    public Cliente buscarClientePorEmail( String email ) {
        return Cliente.find( "email", email ).firstResult();
    }

    public void cadastrarCliente( Cliente cliente ) {
        cliente.persist();
    }

    public void atualizarCliente( Cliente cliente ) {
        Cliente clienteExistente = Cliente.findById( cliente.id );
        clienteExistente.nome = cliente.nome;
        clienteExistente.cpf = cliente.cpf;
        clienteExistente.email = cliente.email;
    }
}