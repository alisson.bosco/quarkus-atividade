
package org.acme.services;

import javax.enterprise.context.ApplicationScoped;
import org.acme.entites.Pedido;
import java.util.List;

@ApplicationScoped
public class PedidoService {

    public List< Pedido > listarPedidos() {
        return Pedido.listAll();
    }

    public List< Pedido > listarPedidosPorStatus( String status ) {
        return Pedido.list( "status", status );
    }

}