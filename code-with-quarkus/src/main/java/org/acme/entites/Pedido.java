
package org.acme.entites;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import java.time.LocalDateTime;
import javax.persistence.Entity;

@Entity
public class Pedido extends PanacheEntity {

    public Cliente cliente;
    public String status;
    public LocalDateTime data;

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente( Cliente cliente ) {
        this.cliente = cliente;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus( String status ) {
        this.status = status;
    }

    public LocalDateTime getData() {
        return data;
    }

    public void setData( LocalDateTime data ) {
        this.data = data;
    }
}