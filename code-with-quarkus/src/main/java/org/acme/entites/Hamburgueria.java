// Hamburgueria.java
package org.acme.entites;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import javax.persistence.Entity;

@Entity
public class Hamburgueria extends PanacheEntity {

    public String nome;

    public String endereco;

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco( String endereco ) {
        this.endereco = endereco;
    }

    public String getNome() {
        return nome;
    }

    public void setNome( String nome ) {
        this.nome = nome;
    }
}