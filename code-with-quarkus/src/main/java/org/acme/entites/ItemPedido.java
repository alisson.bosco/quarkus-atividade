
package org.acme.entites;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import javax.persistence.Entity;

@Entity
public class ItemPedido extends PanacheEntity {

    public LancheBebida lancheBebida;
    public Integer quantidade;

    public LancheBebida getLancheBebida() {
        return lancheBebida;
    }

    public void setLancheBebida( LancheBebida lancheBebida ) {
        this.lancheBebida = lancheBebida;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade( Integer quantidade ) {
        this.quantidade = quantidade;
    }

}
