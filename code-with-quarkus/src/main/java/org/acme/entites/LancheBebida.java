package org.acme.entites;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import javax.persistence.Entity;

@Entity
public class LancheBebida extends PanacheEntity {

    public String nome;
    public Double preco;

    public String getNome() {
        return nome;
    }

    public void setNome( String nome ) {
        this.nome = nome;
    }

    public Double getPreco() {
        return preco;
    }

    public void setPreco( Double preco ) {
        this.preco = preco;
    }

}
